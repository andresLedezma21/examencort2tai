import express, { Router } from 'express';
export const router = express.Router();


export default {router}

router.get('/', (req, res) => {
    const params = {
        numDocente:req.query.numDocente,
        nombre:req.query.nombre,
        domicilio:req.query.domicilio,
        nivel:req.query.nivel,
        pagoHrBase:req.query.pagoHrBase,
        hrsImpar:req.query.hrsImpar,
        nHijos:req.query.nHijos
    }
    res.render('index',params);
});

router.post('/', (req, res) => {
    const params = {
        numDocente:req.body.numDocente,
        nombre:req.body.nombre,
        domicilio:req.body.domicilio,
        nivel:req.body.nivel,
        pagoHrBase:req.body.pagoHrBase,
        hrsImpar:req.body.hrsImpar,
        nHijos:req.body.nHijos
    }
    res.render('resultado',params);
});


